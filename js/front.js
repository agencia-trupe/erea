$('document').ready( function(){

	Modernizr.load([
		{
			test: Modernizr.input.placeholder,
		  	nope: 'js/polyfill-placeholder.js'
		}
	]);

	if($('#frase-destaque').length){
		var span = $('#frase-destaque span');
		var l_final = span.width() * -1.1;
		var l_inicial = $(window).width();
		var duracao = 30000;
		var duracao_intervalo = 30100;
		
		span.css('left', l_inicial);
		span.hover( function(){
	        span.stop(true,false);
	    }, function(){
	        var cur = parseInt(span.css('left'));
	        span.animate({ "left": l_final }, ((cur + Math.abs(l_final)) * duracao) / (l_inicial + Math.abs(l_final)) , 'linear');
	    });

		span.animate({
			'left' : l_final
		}, duracao, 'linear', function(){
			span.css('left', l_inicial);			
		});

		setInterval( function(){
			span.animate({
				'left' : l_final
			}, duracao, 'linear', function(){
				span.css('left', l_inicial);			
			})
		}, duracao_intervalo);
		
	}

	var altura = $(window).height();
	var largura = $(window).width();
	altura = altura - $('header').height();
	if($('.main-contato').length){
		$('.main').css('min-height', altura);
		$('.main .coluna.mapa').css('height', altura);
	}
	else{
		$('.main').css('min-height', altura);
	}

	window.onresize = function(event) {    
		var altura = $(window).height();
		var largura = $(window).width();
		altura = altura - $('header').height();
		if($('.main-contato').length){
			$('.main').css('min-height', altura);
		}else{
			$('.main').css('min-height', altura);
			$('#slides').css('height', altura);
			$('#slides .slide').css('height', altura);
		}		
	};

	if($('#slides').length){

		$('#slides').css('height', altura);

		var slides = JSON.parse($('#slides').attr('data-slides'));
		var img = [];
		var slide = [];

		for (var i = 0; i < slides.length; i++) {
			var imgsrc = slides[i];
			bgImg = new Image();
			bgImg.onload = function(){
				$("#slides").append($("<div class='slide' style='background-image:url("+this.src+");'></div>"));
				$('#slides .slide').css('height', altura);
				$('#slides').cycle();
			};
			bgImg.src = "_imgs/slides/"+imgsrc;
		};
	}

	$('nav #mn-produtos.comsub > a').click( function(e){ e.preventDefault(); });

	if($('.produtos-container .produtos-slide').length){
		$('header nav ul li#mn-produtos .submenu a').click( function(e){
			e.preventDefault();

			// Remove a classe de selecionado do submenu
			$('.submenu a.subativo').removeClass('subativo');
			$(this).addClass('subativo');

			var img = $("<img src='_imgs/layout/ajax-loader.gif'>");
			$('.nome-categoria span').html(img);

			// Conta o nro de itens da categoria atual
			var imagens = $('.produtos-container .produtos-slide li a');
			var contador = 0;

			// Categoria a ser mostrada
			var str_categoria = $(this).attr('href').split('/');
			str_categoria = str_categoria[2];

			// Intervalo para dar fade nos itens
			var timer = setInterval( function(){

				// Ao término do setInterval fazer
				if(contador==imagens.length){

					// Limpar o Intervalo
					clearInterval(timer);

					// Remover os itens apagados e fazer a requisição ajax
					setTimeout(function(){
						$('.produtos-container .produtos-slide li').remove();
						$.getJSON("produtos/categoria/"+str_categoria, function(resposta){

							$('.nome-categoria span').html(resposta.data.nome_categoria.toUpperCase());

							if(resposta.data.tipo_categoria == 'duplo'){

								var str = "<li class='duplo'>";
								for (var i = 0; i < resposta.data.produtos.length; i++) {
									var prod = resposta.data.produtos[i];
									if(i%2==0 && i>0){ str += "</li><li class='duplo'>"; }
									str += "<a href='produtos/detalhes/"+prod.id+"' class='apagado' title='"+prod.titulo+"'>";
									str += "<img src='_imgs/produtos/thumbswide/"+prod.imagem+"' alt='"+prod.titulo+"'>";
									str += "</a>";
								};
								str += '</li>';

								var larguras = 390;

							}else{

								var str = '<li>';
								for (var i = 0; i < resposta.data.produtos.length; i++) {
									var prod = resposta.data.produtos[i];
									if(i%2==0 && i>0){ str += "</li><li>"; }
									str += "<a href='produtos/detalhes/"+prod.id+"' class='apagado' title='"+prod.titulo+"'>";
									str += "<img src='_imgs/produtos/thumbs/"+prod.imagem+"' alt='"+prod.titulo+"'>";
									str += "</a>";
								};
								str += '</li>';

								var larguras = 195;

							}

							$('.produtos-container .produtos-slide').html(str);

							var contador_novos = 0;
							var imagens = $('.produtos-container .produtos-slide li a');

							var largura_imagens = larguras * $('.produtos-container .produtos-slide li').length;
							var distancia_lateral = (parseInt($('.produtos-container').css('width')) - 980) / 2;

							// Se .produtos-container li for maior que a largura do navegador, esconder slider
							if(parseInt($('.produtos-container').css('width')) - (largura_imagens + distancia_lateral) > 0){
								esconder = true;
								$('.produtos-container').css('left', distancia_lateral);
							}else{
								var tamMaximo = parseInt($('.produtos-slide').css('width')) - parseInt($('.produtos-container').css('width'));

								if(tamMaximo <= 0){
									esconder = true;
								}else{
									esconder = false;
								}
								$('#slider').slider({max : tamMaximo});
								$('.produtos-container').css('left', 0);
							}

							var timer_novo = setInterval( function(){

								if(contador_novos==resposta.data.produtos.length){
									clearInterval(timer_novo);

									if(esconder){
										$('#slider').hide();
									}else{
										$('#slider').show();
									}
									window.location.hash = str_categoria;
								}

								$(imagens[contador_novos]).removeClass('apagado');
								contador_novos++;
							}, resposta.data.produtos.length*10);
						});
					}, imagens.length*10);

				}

				// Apaga o item
				$(imagens[contador]).addClass('apagado');
				contador++;

			}, 30);
		});
	}

	if(window.location.hash){
		if($('#sub-'+window.location.hash).length){
			$('#sub-'+window.location.hash).click();
		}else if($('#sub-'+window.location.hash.substring(1)).length){
			$('#sub-'+window.location.hash.substring(1)).click();
		}
	}

	if($('#slider').length){
		tamMaximo = parseInt($('.produtos-slide').css('width')) - parseInt($('.produtos-container').css('width'));
		if(tamMaximo > 0){
		    $('#slider').slider({
		        min : 0,
		        max : tamMaximo,
		        animate : 'normal',
		        slide : function(event, ui){
		            $('.produtos-container').stop().animate({'left' : -ui.value}, 'fast');
		        }
		    });
		}else{
			$('#slider').hide();
			var distancia_lateral = (parseInt($('.produtos-container').css('width')) - 980) / 2;
			$('.produtos-container').css('left', distancia_lateral);
		}
	}



	if($('#variacoes').length){
		if($('#variacoes .thumb').length < 3){
			$('#variacoes .nav').hide();
		}else{
			$('#variacoes .nav.nav-up').click( function(e){
				e.preventDefault();
				if(!$('#variacoes').hasClass('animando')){
					if(parseInt($('#variacoes .thumb-viewport li:first-child').css('margin-top')) < 0){
						$('#variacoes').addClass('animando');
						$('#variacoes .thumb-viewport li:first-child').css('margin-top', parseInt($('#variacoes .thumb-viewport li:first-child').css('margin-top')) + 147);
						setTimeout( function(){$('#variacoes').removeClass('animando');}, 310);
					}
				}
			});
			$('#variacoes .nav.nav-down').click( function(e){
				e.preventDefault();
				if(!$('#variacoes').hasClass('animando')){
					if(parseInt($('#variacoes .thumb-viewport li:first-child').css('margin-top')) > ($('#variacoes .thumb-viewport li').length - 3) * -147){
						$('#variacoes').addClass('animando');
						$('#variacoes .thumb-viewport li:first-child').css('margin-top', parseInt($('#variacoes .thumb-viewport li:first-child').css('margin-top')) - 147);
						setTimeout( function(){$('#variacoes').removeClass('animando');}, 310);
					}
				}
			});
		}
		$('#variacoes .thumb').click( function(e){
			e.preventDefault();
			$('#variacoes .thumb.variacao-ativo').removeClass('variacao-ativo');
			$(this).addClass('variacao-ativo');
			var src = $(this).attr('href');
			$('#ampliada img').attr('src', src);
		});
	}



	$('#form-contato').submit( function(){
		if($("input[name='nome']").val() == '' || $("input[name='nome']").val() == $("input[name='nome']").attr('placeholder')){
			alert('Informe seu nome');
			return false;
		}
		if($("input[name='email']").val() == '' || $("input[name='email']").val() == $("input[name='email']").attr('placeholder')){
			alert('Informe seu e-mail');
			return false;
		}
		if($("textarea").val() == '' || $("textarea").val() == $("textarea").attr('placeholder')){
			alert('Informe sua mensagem');
			return false;
		}
	});

	$('#vermais-novidades').click( function(e){
		e.preventDefault();
		$('.loading').removeClass('fechado');
		var ultimo = $('ul.lista-novidades li:last').attr('data-id');
		$.getJSON("ajax/pegarNovidades/"+ultimo, function(resposta){
			if(resposta.retorno.retorno == 1){
				for (var i = 0; i < resposta.retorno.resultados.length; i++) {
					var s = resposta.retorno.resultados[i];

					var elemento_str = "<li data-id='"+s.indice+"' class='fechado'>";

					if(s.imagem)
						elemento_str += "<img src='_imgs/novidades/"+s.imagem+"' alt='"+s.titulo+"'>";

					if(s.data_divulgacao != '00/00/0000')
						elemento_str += "<div class='data'>"+s.data_divulgacao+"</div>";

					elemento_str += "<h1>"+s.titulo+"</h1>";
					elemento_str += "<div class='texto'>"+s.texto+"</div>";
					elemento_str += "</li>";
					$('ul.lista-novidades').append($(elemento_str));
				};

				if($('ul.lista-novidades li').length >= resposta.retorno.total)
					$('#vermais-novidades').hide();
			}
			$('ul.lista-novidades li.fechado').removeClass('fechado');
			$('.loading').addClass('fechado');
		});
	});

	$('#vermais-lifestyle').click( function(e){
		e.preventDefault();

		var botao = $(this);

		if(!botao.hasClass('atualizando')){

			botao.addClass('atualizando');

			$('.loading').removeClass('fechado');			
			$('html, body').css('overflow-x', 'hidden');

			var pagina_atual = $('#pagina-atual').val();
			var lista_atual = $('ul.lista-lifestyle');

			$.getJSON("ajax/pegarLifestyle/"+pagina_atual, function(resposta){
			
				var cont_h = 1;console.log(resposta)
				var cont_v = 1;
				var nova_lista = $("<ul class='lista-lifestyle pre'></ul>");
			
				if(resposta.retorno.num_resultados > 0){
					
					$('#pagina-atual').val(resposta.retorno.pagina);

					for (var i = 0; i < resposta.retorno.num_resultados; i++) {
						if(resposta.retorno.resultados[i].orientacao == 'h'){
							var el = $("<li class='h h-"+cont_h+"'><img src='_imgs/lifestyle/"+resposta.retorno.resultados[i].imagem+"' alt='Lifestyle Erea'></li>");
							cont_h++;
						}else{
							var el = $("<li class='v v-"+cont_v+"'><img src='_imgs/lifestyle/"+resposta.retorno.resultados[i].imagem+"' alt='Lifestyle Erea'></li>");
							cont_v++;
						}
						nova_lista.append(el);
					};

					lista_atual.after(nova_lista);
					lista_atual.addClass('pos');

					setTimeout( function(){
						nova_lista.removeClass('pre');
						$(".lista-lifestyle.pos").remove();
						botao.removeClass('atualizando');
					}, 200);
				}

				$('html, body').css('overflow-x', 'auto');
				$('.loading').addClass('fechado');
			});
		}
	});
});