var Admin = {

  toggleLoginRecovery: function(){
    var is_login_visible = $('#modal-login').is(':visible');
    (is_login_visible ? $('#modal-login') : $('#modal-recovery')).slideUp(300, function(){
      (is_login_visible ? $('#modal-recovery') : $('#modal-login')).slideDown(300, function(){
        $(this).find('input:text:first').focus();
      });
    });
  },

  scrollTop: function(){
    $('html, body').animate({
      'scrollTop' : 0
    }, 300);
  },

  voltar: function(){
    window.history.back();
  },

  mostraAlerta: function(){
    var alerta = $('.alert');
    setTimeout( function(){
      alerta.slideUp(400, function(){
        alerta.remove();
      })
    }, 4000);
  },

  mostraBotaoScroll: function(){
    var hasVScroll = parseInt($('body').css('height')) > window.innerHeight;
    if(!hasVScroll)
      $('#footer .inner .container .right a').hide();
  },

  marcarLancamento: function(parent, acao, id_produto){
    $.post(BASE+'/ajax/marcarLancamento/', {
      acao : acao,
      id_produto : id_produto
    }, function(retorno){

      if(acao == 1){
        var novo_botao = "<a href='#' class='btn btn-success btn-xs btn-marcar-lancamento' data-action='0' data-produto='"+id_produto+"' title='Desmarcar lançamento'><i class='icon-white icon-star'></i> lançamento</a>";
      }else{
        var novo_botao = "<a href='#' class='btn btn-default btn-xs btn-marcar-lancamento' data-action='1' data-produto='"+id_produto+"' title='Marcar como lançamento'><i class='icon-star-empty'></i> lançamento</a>";
      }

      parent.html(novo_botao);
    });
  }


};

$(function(){

  $('#id_produtos_categorias').change( function(){
    var id_cat = $(this).val();
    $.post(BASE+'/ajax/tipoCategoria/', {id_categoria : id_cat}, function(retorno){
      retorno = JSON.parse(retorno);
      if(retorno.erro == 0){
        $('#appended-lbl-img').remove();
        if(retorno.valor == 'duplo')
          $('#lbl-img').append("<span id='appended-lbl-img'> - Categoria com imagens de largura dupla (390px)</span>");
        else
          $('#lbl-img').append("<span id='appended-lbl-img'> - Categoria com imagens de largura simples (195px)</span>");
      }
    });
  });

  $('body').on('click', '.btn-marcar-lancamento', function(e){
    e.preventDefault();

    var parent = $(this).parent();
    var acao = $(this).attr('data-action');
    var id_produto = $(this).attr('data-produto');

    Admin.marcarLancamento(parent, acao, id_produto);

  });

  $('.toggle-login-recovery').click(function(e){
    e.preventDefault();
    Admin.toggleLoginRecovery();
  });

  $('#footer .inner .container .right a').click( function(e){
    e.preventDefault();
    Admin.scrollTop();
  });

  $('.btn-move').click( function(e){
    e.preventDefault();
  });

  $('.btn-delete').click( function(e){
    e.preventDefault();
      var destino = $(this).attr('href');
      bootbox.confirm("Deseja Excluir o Registro?", function(result){
        if(result)
          window.location = destino;
      });
  });

  $('.btn-voltar').click( function(){
      Admin.voltar();
  });

  if($('.alert').length){
      Admin.mostraAlerta();
  }

  $("table.table-sortable tbody").sortable({
      update : function () {
          serial = [];
          tabela = $('table.table-sortable').attr('data-tabela');
          $('table.table-sortable tbody').children('tr').each(function(idx, elm) {
              serial.push(elm.id.split('_')[1])
          });
          $.post(BASE+'/ajax/gravaOrdem', { data : serial , tabela : tabela });
      },
      helper: function(e, ui) {
        ui.children().each(function() {
          $(this).width($(this).width());
        });
        return ui;
      },
      handle : $('.btn-move')
  }).disableSelection();

  $('.datepicker').datepicker();

  $('.monthpicker').monthpicker();

  Admin.mostraBotaoScroll();

  tinyMCE.init({
      language : "pt",
      mode : "specific_textareas",
      editor_selector : "imagem",
      theme : "advanced",
      theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,separator,image,fontsizeselect",
      theme_advanced_buttons2 : "",
      theme_advanced_buttons3 : "",
      theme_advanced_blockformats : "h1, h2, p",
      theme_advanced_toolbar_location : "top",
      theme_advanced_statusbar_location : 'bottom',
      theme_advanced_path : false,
      plugins: "paste,advimage",
      paste_text_sticky : true,
      theme_advanced_resizing : true,
      setup : function(ed) {
          ed.onInit.add(function(ed) {
              ed.getDoc().body.style.fontSize = '14px';
              ed.pasteAsPlainText = true;
          });
      },
      theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
      font_size_style_values : "10px,12px,13px,14px,16px,18px",
      content_css : BASE+"/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  tinyMCE.init({
      language : "pt",
      mode : "specific_textareas",
      editor_selector : "completo",
      theme : "advanced",
      theme_advanced_buttons1 : "formatselect,separator,bold,italic,underline,separator,link,unlink,fontsizeselect",
      theme_advanced_buttons2 : "",
      theme_advanced_buttons3 : "",
      theme_advanced_blockformats : "h1, h2, p",
      theme_advanced_toolbar_location : "top",
      theme_advanced_statusbar_location : 'bottom',
      theme_advanced_path : false,
      plugins: "paste",
      paste_text_sticky : true,
      theme_advanced_resizing : true,
      setup : function(ed) {
          ed.onInit.add(function(ed) {
              ed.getDoc().body.style.fontSize = '14px';
              ed.pasteAsPlainText = true;
          });
      },
      theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
      font_size_style_values : "10px,12px,13px,14px,16px,18px",
      content_css : BASE+"/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

  tinyMCE.init({
      language : "pt",
      mode : "specific_textareas",
      editor_selector : "basico",
      theme : "advanced",
      theme_advanced_buttons1 : "bold,italic,underline,link,unlink,fontsizeselect",
      theme_advanced_buttons2 : "",
      theme_advanced_buttons3 : "",
      theme_advanced_toolbar_location : "top",
      theme_advanced_statusbar_location : 'bottom',
      theme_advanced_path : false,
      plugins: "paste",
      paste_text_sticky : true,
      theme_advanced_resizing : true,
      setup : function(ed) {
          ed.onInit.add(function(ed) {
              ed.getDoc().body.style.fontSize = '14px';
              ed.pasteAsPlainText = true;
          });
      },
      theme_advanced_font_sizes: "10px,12px,13px,14px,16px,18px",
      font_size_style_values : "10px,12px,13px,14px,16px,18px",
      content_css : BASE+"/../css/painel/css/tinymce.css?nocache=" + new Date().getTime()
  });

});
