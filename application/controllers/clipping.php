<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clipping extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
   		$this->load->model('clippings_model', 'clippings');
    }

    function index(){
    	$data['clippings'] = $this->clippings->pegarTodos();
   		$this->load->view('clipping/index', $data);
    }

    function ler($id = false){
    	if(!$id)
    		redirect('clipping/index/');

    	$data['detalhes'] = $this->clippings->pegarPorId($id);
    	$data['imagens'] = $this->clippings->imagens($id);
    	$this->load->view('clipping/detalhes', $data);
    }

}