<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

    function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('home');
    }

    function pegarNovidades($inicio = 9){
    	$por_pagina = 9;
    	$query = $this->db->order_by('data_ordenacao', 'desc')->get('novidades', $por_pagina, $inicio)->result();
    	$num_resultados = $this->db->order_by('data_ordenacao', 'desc')->get('novidades')->num_rows();

    	if ($query) {
	    	foreach ($query as $key => $value) {
	    		$value->indice = $inicio + $key + 1;
	    		$value->data_divulgacao = formataData($value->data_divulgacao, 'mysql2br');
	    	}
	    	$retorno = array(
	    		'retorno' => 1,
	    		'num_resultados' => sizeof($query),
	    		'resultados' => $query,
	    		'total' => $num_resultados
	    	);
    	}else{
    		$retorno = array(
	    		'retorno' => 0,
	    		'num_resultados' => 0,
	    		'resultados' => 0,
	    		'total' => $num_resultados
	    	);
    	}

    	$this->output->set_content_type('application/json')->set_output(json_encode(array('retorno' => $retorno)));
    }

    function pegarLifestyle($pagina_atual){    
    	$pagina = $pagina_atual + 1;

    	$inicio_h = $pagina * 4;
    	$inicio_v = $pagina * 3;

        $query_h = $this->db->query("SELECT * FROM lifestyle_imagens WHERE orientacao = 'h' LIMIT $inicio_h, 4")->result();
        $query_v = $this->db->query("SELECT * FROM lifestyle_imagens WHERE orientacao = 'v' LIMIT $inicio_v, 3")->result();
        $merge = array_merge($query_h,$query_v);

        if(sizeof($merge) == 0){

        	$query_h = $this->db->query("SELECT * FROM lifestyle_imagens WHERE orientacao = 'h' LIMIT 0, 4")->result();
        	$query_v = $this->db->query("SELECT * FROM lifestyle_imagens WHERE orientacao = 'v' LIMIT 0, 3")->result();

        	$merge = array_merge($query_h,$query_v);

			$retorno = array(
	    		'retorno' => $query_h || $query_v,
	    		'num_resultados' => sizeof($merge),
	    		'resultados' => $merge,
	    		'pagina' => 0
	    	);
        }else{
        	$retorno = array(
	    		'retorno' => $query_h || $query_v,
	    		'num_resultados' => sizeof($merge),
	    		'resultados' => $merge,
	    		'pagina' => $pagina
	    	);
        }
        
        $this->output->set_content_type('application/json')->set_output(json_encode(array('retorno' => $retorno)));
    }
}
