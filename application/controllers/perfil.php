<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('perfil_model', 'perfil');

    	$qry = $this->perfil->pegarTodos();

    	if (isset($qry[0]) && $qry[0]) {
    		$data['perfil'] = $qry[0];
    	}

   		$this->load->view('perfil', $data);
    }

}