<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$por_pagina = 9;
    	$data['lista'] = $this->db->order_by('data_ordenacao', 'desc')->get('novidades', $por_pagina, 0)->result();
    	$data['num_resultados'] = $this->db->order_by('data_ordenacao', 'desc')->get('novidades')->num_rows();
    	$data['paginar'] = $data['num_resultados'] > $por_pagina;
   		$this->load->view('novidades/index', $data);
    }

}