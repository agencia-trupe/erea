<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lancamentos extends MY_Frontcontroller {

  function __construct(){
    parent::__construct();
  }

  function index(){

    $data['produtos'] = $this->produtos->pegarLancamentos();

    if(!$data['produtos'])
      redirect('produtos/index/');

    $this->menuvar['slug'] = 'colecao-2017';
    $this->headervar['load_css'] = 'produtos';

    //$data['nome_categoria'] = 'Lançamentos';
    $data['nome_categoria'] = 'Coleção 2018';
    $data['tipo_categoria'] = 'duplo';

    if($this->input->is_ajax_request()){
      $this->hasLayout = FALSE;
      $this->output
          ->set_content_type('application/json')
          ->set_output(json_encode(array('data' => $data)));
    }else{
     $this->load->view('produtos/index', $data);
    }
  }

}
