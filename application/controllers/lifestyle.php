<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lifestyle extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$por_pagina = 7;
    	
        $data['pagina'] = 0;

        $data['lista'] = $this->pegarImagens();
    	$data['num_resultados'] = $this->db->order_by('id', 'desc')->get('lifestyle_imagens')->num_rows();
    	$data['paginar'] = $data['num_resultados'] > $por_pagina;

        $data['texto'] = $this->db->get('lifestyle_texto')->result();
   		$this->load->view('lifestyle', $data);
    }

    function pegarImagens($inicio_h = 0, $inicio_v = 0){
        $query_h = $this->db->query("SELECT * FROM lifestyle_imagens WHERE orientacao = 'h' LIMIT $inicio_h, 4")->result();
        $query_v = $this->db->query("SELECT * FROM lifestyle_imagens WHERE orientacao = 'v' LIMIT $inicio_v, 3")->result();

        return array_merge($query_h,$query_v);
    }

}