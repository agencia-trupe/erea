<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
    	$this->load->model('contato_model', 'contato');

    	$qry = $this->contato->pegarTodos();
    	$data['contato'] = (isset($qry[0])) ? $qry[0] : false;
   		$this->load->view('contato', $data);
    }

    function enviar(){
	    $nome = $this->input->post('nome');
	    $email = $this->input->post('email');
	    $telefone = $this->input->post('telefone');
	    $mensagem = $this->input->post('mensagem');

	    if($nome && $email && $mensagem){
	        $emailconf['charset'] = 'utf-8';
	        $emailconf['mailtype'] = 'html';
	        
	        $emailconf['protocol'] = 'smtp';
	        $emailconf['smtp_host'] = 'smtp.erea.com.br';
	        $emailconf['smtp_user'] = 'noreply@erea.com.br';
	        $emailconf['smtp_pass'] = 'semresposta15';
	        $emailconf['smtp_port'] = 587;
			
	        $this->load->library('email');

	        $this->email->initialize($emailconf);

	        $from = 'noreply@erea.com.br';
	        $fromname = 'Érea';
	        $to = 'erea@erea.com.br';
	        $bcc = false;
	        $assunto = 'Contato via Site';

	        $email = <<<EML
<!DOCTYPE html>
<html>
<head>
    <title>Mensagem de contato via formulário do site</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Nome :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$nome</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>E-mail :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$email</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Telefone :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$telefone</span><br>
    <span style='font-weight:bold;font-size:16px;color:#00B69C;font-family:Verdana;'>Mensagem :</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>$mensagem</span>
</body>
</html>
EML;

	        $plain = <<<EML
Nome :$nome\r\n
E-mail :$email\r\n
Telefone :$telefone\r\n
Mensagem :$mensagem
EML;

	        $this->email->from($from, $fromname);
	        $this->email->to($to);
	        if($bcc)
	            $this->email->bcc($bcc);
	        $this->email->reply_to($email);

	        $this->email->subject($assunto);
	        $this->email->message($email);
	        $this->email->set_alt_message($plain);

	        $this->email->send();
	    }

		$this->session->set_flashdata('contato_ok', true);
		redirect('contato/index');
    }
}