<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destaque extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Destaque da Home";
		$this->unidade = "Frase";
		$this->load->model('destaque_model', 'model');
	}

}