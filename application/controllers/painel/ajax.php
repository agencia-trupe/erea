<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller {

   function __construct(){
   		parent::__construct();

   		if(!$this->input->is_ajax_request())
   			redirect('painel/home');
   }

	function gravaOrdem(){
        $menu = $this->input->post('data');
        for ($i = 0; $i < count($menu); $i++) {
            $this->db->set('ordem', $i)
            		 ->where('id', $menu[$i])
            		 ->update($this->input->post('tabela'));
        }
	}

  function marcarLancamento(){
    $this->db
         ->set('lancamento', $this->input->post('acao'))
         ->where('id', $this->input->post('id_produto'))
         ->update('produtos');
  }

  function tipoCategoria() {
    $id_cat = $this->input->post('id_categoria');
    $qry = $this->db->get_where('produtos_categorias', array('id' => $id_cat))->result();
    if(isset($qry[0]))
      echo json_encode(array('erro' => 0, 'valor' => $qry[0]->tipo_thumbnail));
    else
      echo json_encode(array('erro' => 1, 'valor' => ''));
  }

}
