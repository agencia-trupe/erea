<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Produtos";
		$this->unidade = "Produto";
		$this->load->model('produtos_model', 'model');
	}

    function index($categoria = false, $pag = 0){
        $this->load->library('pagination');

        if (!$categoria) {
        	$qry_categoria = $this->model->categorias();
        	if (isset($qry_categoria[0])) {
        		$categoria = $qry_categoria[0]->id;
        	}
        }

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/index/".$categoria.'/'),
            'per_page' => 2000,
            'uri_segment' => 5,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroResultados($categoria)
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarPaginadoPorCategoria($pag_options['per_page'], $pag, 'ordem', 'asc', $categoria);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['categorias'] = $this->model->categorias();
        $data['categoria_atual'] = $categoria;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

		function lancamentos(){

        $data['registros'] = $this->model->pegarLancamentos();

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = 'Lançamentos';
        $data['unidade'] = $this->unidade;
        $data['categorias'] = $this->model->categorias();
        $data['categoria_atual'] = 'lancamentos';
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

    function form($id = false){
        if($id){
            $data['registro'] = $this->model->pegarPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class);
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $data['categorias'] = $this->model->categorias();
        $this->load->view('painel/'.$this->router->class.'/form', $data);
    }

    function inserir(){
        if($this->model->inserir()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' inserido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index/'.$this->input->post('id_produtos_categorias'), 'refresh');
    }

    function alterar($id){
        if($this->model->alterar($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' alterado com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index/'.$this->input->post('id_produtos_categorias'), 'refresh');
    }

    function excluir($id, $categoria){
        if($this->model->excluir($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', $this->unidade.' excluido com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir '.$this->unidade);
        }

        redirect('painel/'.$this->router->class.'/index/'.$categoria, 'refresh');
    }

    function categorias($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
            'base_url' => base_url("painel/".$this->router->class."/categorias/"),
            'per_page' => 20,
            'uri_segment' => 4,
            'next_link' => "Próxima →",
            'next_tag_open' => "<li class='next'>",
            'next_tag_close' => '</li>',
            'prev_link' => "← Anterior",
            'prev_tag_open' => "<li class='prev'>",
            'prev_tag_close' => '</li>',
            'display_pages' => TRUE,
            'num_links' => 10,
            'first_link' => FALSE,
            'last_link' => FALSE,
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            'cur_tag_open' => '<li><b>',
            'cur_tag_close' => '</b></li>',
            'total_rows' => $this->model->numeroCategorias()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->categorias(false, $pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;

        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista-categorias', $data);
    }

    function formCategoria($id = false){
        if($id){
            $data['registro'] = $this->model->categorias($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class.'/categorias');
        	$data['titulo'] = "Alterar Categoria";
        }else{
        	$data['titulo'] = "Inserir Categoria";
            $data['registro'] = FALSE;
        }

        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form-categorias', $data);
    }

    function inserirCategoria(){
        if($this->model->inserirCategoria()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function alterarCategoria($id){
        if($this->model->alterarCategoria($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }

    function excluirCategoria($id){
        if($this->model->excluirCategoria($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Categoria excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Categoria');
        }

        redirect('painel/'.$this->router->class.'/categorias', 'refresh');
    }
}
