<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categorias extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Categorias";
		$this->unidade = "Categoria";
		$this->load->model('categorias_model', 'model');
    }

}
