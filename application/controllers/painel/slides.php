<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Slides extends MY_Admincontroller {

   function __construct(){
   	parent::__construct();

   	$this->load->model('slides_model', 'model');
   }

   function index($pag = 0){

      $data['registros'] = $this->model->pegarTodos();

      $data['titulo'] = "Slides";
      $data['unidade'] = "Slide";
      
      if($this->session->flashdata('mostrarerro') === true)
         $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
      else
         $data['mostrarerro'] = false;
     
      if($this->session->flashdata('mostrarsucesso') === true)
         $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
      else
         $data['mostrarsucesso'] = false;

      $data['tabela_ordenacao'] = $this->model->tabela();

   	$this->load->view('painel/slides/lista', $data);
   }

   function form($id = false){

      if($id){
         $data['titulo'] = 'Editar Slides';
   	   $data['registro'] = $this->model->pegarPorId($id);
         if(!$data['registro'])
            redirect('painel/slides');
      }else{
         $data['titulo'] = 'Inserir Slides';
         $data['registro'] = FALSE;
      }
      
      $data['unidade'] = "Slides";
   	$this->load->view('painel/slides/form', $data);
   }

   function inserir(){
      if($this->model->inserir()){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Slides inserido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Slides');
      }

   	redirect('painel/slides/index/', 'refresh');
   }

   function alterar( $id){
      if($this->model->alterar($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Slides alterado com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Banner');
      }     
   	redirect('painel/slides/index/', 'refresh');
   }

   function excluir($id){
	   if($this->model->excluir($id)){
         $this->session->set_flashdata('mostrarsucesso', true);
         $this->session->set_flashdata('mostrarsucesso_mensagem', 'Banner excluido com sucesso');
      }else{
         $this->session->set_flashdata('mostrarerro', true);
         $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Slides');
      }

      redirect('painel/slides/index/', 'refresh');
   }

}