<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clippings extends MY_Admincontroller {

    function __construct(){
   		parent::__construct();

   		$this->titulo = 'Clippings';
   		$this->load->model('clippings_model', 'model');
    }

    function index($pag = 0){
        $this->load->library('pagination');

          $pag_options = array(
             'base_url' => base_url("painel/".$this->router->class."/index/"),
             'per_page' => 2000,
             'uri_segment' => 4,
             'next_link' => "Próxima →",
             'next_tag_open' => "<li class='next'>",
             'next_tag_close' => '</li>',
             'prev_link' => "← Anterior",
             'prev_tag_open' => "<li class='prev'>",
             'prev_tag_close' => '</li>',
             'display_pages' => TRUE,
             'num_links' => 10,
             'first_link' => FALSE,
             'last_link' => FALSE,
             'num_tag_open' => '<li>',
             'num_tag_close' => '</li>',
             'cur_tag_open' => '<li><b>',
             'cur_tag_close' => '</b></li>',
             'total_rows' => $this->model->numeroResultados()
          );

          $this->pagination->initialize($pag_options);
          $data['paginacao'] = $this->pagination->create_links();

          $data['registros'] = $this->model->pegarPaginado($pag_options['per_page'], $pag);

          if($this->session->flashdata('mostrarerro') === true)
             $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
          else
             $data['mostrarerro'] = false;
         
          if($this->session->flashdata('mostrarsucesso') === true)
             $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
          else
             $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista', $data);
    }

}