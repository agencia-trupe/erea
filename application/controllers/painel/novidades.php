<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Novidades";
		$this->unidade = "Novidade";
		$this->load->model('novidades_model', 'model');
	}

}