<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lifestyle extends MY_Admincontroller {

	function __construct(){
		parent::__construct();

		$this->titulo = "Lifestyle";
		$this->unidade = "Texto";
		$this->load->model('lifestyle_model', 'model');
	}

    function imagens($pag = 0){
        $this->load->library('pagination');

        $pag_options = array(
             'base_url' => base_url("painel/".$this->router->class."/imagens/"),
             'per_page' => 20,
             'uri_segment' => 4,
             'next_link' => "Próxima →",
             'next_tag_open' => "<li class='next'>",
             'next_tag_close' => '</li>',
             'prev_link' => "← Anterior",
             'prev_tag_open' => "<li class='prev'>",
             'prev_tag_close' => '</li>',
             'display_pages' => TRUE,
             'num_links' => 10,
             'first_link' => FALSE,
             'last_link' => FALSE,
             'num_tag_open' => '<li>',
             'num_tag_close' => '</li>',
             'cur_tag_open' => '<li><b>',
             'cur_tag_close' => '</b></li>',
             'total_rows' => $this->model->numeroImagens()
        );

        $this->pagination->initialize($pag_options);
        $data['paginacao'] = $this->pagination->create_links();

        $data['registros'] = $this->model->pegarImagensPaginado($pag_options['per_page'], $pag);

        if($this->session->flashdata('mostrarerro') === true)
            $data['mostrarerro'] = $this->session->flashdata('mostrarerro_mensagem');
        else
            $data['mostrarerro'] = false;
         
        if($this->session->flashdata('mostrarsucesso') === true)
            $data['mostrarsucesso'] = $this->session->flashdata('mostrarsucesso_mensagem');            
        else
            $data['mostrarsucesso'] = false;

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/lista-imagens', $data);
    }

    function formImagem($id = false){
        if($id){
            $data['registro'] = $this->model->pegarImagemPorId($id);
            if(!$data['registro'])
                redirect('painel/'.$this->router->class.'/imagens');
        }else{
            $data['registro'] = FALSE;
        }

        $data['titulo'] = $this->titulo;
        $data['unidade'] = $this->unidade;
        $this->load->view('painel/'.$this->router->class.'/form-imagens', $data);
    }

    function inserirImagem(){
        if($this->model->inserirImagem()){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem inserida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao inserir Imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens', 'refresh');
    }

    function alterarImagem($id){
        if($this->model->alterarImagem($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem alterada com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao alterar Imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens', 'refresh');
    }

    function excluirImagem($id){
        if($this->model->excluirImagem($id)){
            $this->session->set_flashdata('mostrarsucesso', true);
            $this->session->set_flashdata('mostrarsucesso_mensagem', 'Imagem excluida com sucesso');
        }else{
            $this->session->set_flashdata('mostrarerro', true);
            $this->session->set_flashdata('mostrarerro_mensagem', 'Erro ao excluir Imagem');
        }

        redirect('painel/'.$this->router->class.'/imagens', 'refresh');
    }
}