<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();

        $this->headervar['load_css'] = 'marquee';
    }

    function index(){
    	$this->load->model('slides_model', 'slides');
        $this->load->model('destaque_model', 'destaque');

    	$data['imagens'] = $this->slides->pegarTodos('ordem', 'asc');

    	$json_images = array();

    	foreach ($data['imagens'] as $key => $value)
    		$json_images[] = $value->imagem;

    	$data['primeiro_slide'] = array_shift($json_images);
    	$data['json_images'] = json_encode($json_images);

        $qry_destaque = $this->destaque->pegarTodos();
        $data['destaque'] = isset($qry_destaque[0]) ? $qry_destaque[0] : false;

   		$this->load->view('home', $data);
    }

}