<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos extends MY_Frontcontroller {

    function __construct(){
   		parent::__construct();
    }

    function index(){
      $data['categorias'] = $this->produtos->categoriasSemColecoes();
   		$this->load->view('produtos/preindex', $data);
    }

    function categoria($slug_categoria){

    	$query_categoria = $this->produtos->categoriasPorSlug($slug_categoria);

    	if(!$query_categoria)
    		redirect('produtos/index/');

    	$data['produtos'] = $this->produtos->pegarPorCategoria($slug_categoria);
    	$this->menuvar['slug'] = $slug_categoria;

    	$data['nome_categoria'] = $query_categoria->titulo;
        $data['tipo_categoria'] = $query_categoria->tipo_thumbnail;

        if($this->input->is_ajax_request()){
            $this->hasLayout = FALSE;
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode(array('data' => $data)));
        }else{
    	   $this->load->view('produtos/index', $data);
        }
    }

    function detalhes($id){
    	$data['detalhes'] = $this->produtos->pegarPorId($id);

    	if(!$data['detalhes'])
    		redirect('produtos/index/');

    	$query_categoria = $this->produtos->categorias($data['detalhes']->id_produtos_categorias);
    	$this->menuvar['slug'] = $query_categoria->slug;
		$data['nome_categoria'] = $query_categoria->titulo;
		$data['slug_categoria'] = $query_categoria->slug;

    	$data['variacoes'] = $this->produtos->imagens($id);
    	$this->load->view('produtos/detalhes', $data);
    }
}
