<?php if ($imagens): ?>
	<div id="slides" data-slides='<?=$json_images?>' <?php if(isset($_GET['alternativa'])){ echo "class='alt'"; } ?>>
		<?if(isset($primeiro_slide)):?>
			<div class="slide" style="background-image:url(_imgs/slides/<?=$primeiro_slide?>);"></div>
		<?endif;?>
	</div>
<?php endif ?>

<?php if ($destaque): ?>
	<?php if ($destaque->ativa == 1): ?>
		<div id="frase-destaque">
			<span><?=$destaque->frase?></span>
		</div>
	<?php endif ?>
<?php endif ?>