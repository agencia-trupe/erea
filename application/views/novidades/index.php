<div class="centro novidades">

	<?php if ($lista): ?>
		<ul class="lista-novidades">

			<?php foreach ($lista as $key => $value): ?>
				<li data-id="<?=($key + 1)?>">

					<?php if ($value->imagem): ?>
						<img src="_imgs/novidades/<?=$value->imagem?>" alt="<?=$value->titulo?>">
					<?php endif ?>

					<?php if ($value->data_divulgacao != '0000-00-00'): ?>
						<div class="data"><?=formataData($value->data_divulgacao,'mysql2br')?></div>
					<?php endif ?>

					<h1><?=$value->titulo?></h1>

					<div class="texto"><?=$value->texto?></div>

				</li>
			<?php endforeach ?>
		</ul>

		<div class="loading fechado"></div>

		<?php if ($paginar): ?>
			<a href="#" id="vermais-novidades" title="Ver mais">ver mais &raquo;</a>
		<?php endif ?>
	<?php endif ?>

	<input type="hidden" id="num-novidades" value="<?=$num_resultados?>">

</div>