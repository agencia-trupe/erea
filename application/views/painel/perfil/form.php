<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Texto 1<br>
		<textarea name="texto" class="pequeno basico"><?=$registro->texto?></textarea></label>

		<label>Texto 2<br>
		<textarea name="perfil1" class="pequeno basico"><?=$registro->perfil1?></textarea></label>

		<label>Texto 3<br>
		<textarea name="perfil2" class="pequeno basico"><?=$registro->perfil2?></textarea></label>

		<label>Imagem Perfil<br>
			<?if($registro->imagem1):?>
				<img src="_imgs/perfil/<?=$registro->imagem1?>"><br>
			<?endif;?>
		<input type="file" name="userfile1"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Texto 1<br>
		<textarea name="texto" class="pequeno basico"></textarea></label>

		<label>Texto 2<br>
		<textarea name="perfil1" class="pequeno basico"></textarea></label>

		<label>Texto 3<br>
		<textarea name="perfil2" class="pequeno basico"></textarea></label>

		<label>Imagem Perfil<br>
		<input type="file" name="userfile1"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>