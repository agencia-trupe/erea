<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?> <a  href="painel/<?=$this->router->class?>/form" class="btn btn-success">Adicionar <?=$unidade?></a>
    </h2>
  </div>

  <a href="painel/produtos/lancamentos" class="btn<?if($categoria_atual=='lancamentos')echo" btn-info"?>" style="margin-bottom:5px;" title="Ver Lançamentos">
    <i class="<? if($categoria_atual=='lancamentos') echo"icon-white icon-star"; else echo"icon-star-empty"; ?>"></i> Ver Lançamentos
  </a>

	<?php if ($categorias): ?>
		<?php foreach ($categorias as $key => $value): ?>
			<a href="painel/produtos/index/<?=$value->id?>" class="btn<?if($categoria_atual==$value->id)echo" btn-info"?>" style="margin-bottom:5px;" title="Ver produtos com categoria <?=$value->titulo?>"><?=$value->titulo?></a>
		<?php endforeach ?>
	<?php endif ?>

  <hr>

  <div class="row">
    <div class="span12 columns">

      <?php if ($registros): ?>

        <table class="table table-striped table-bordered table-condensed table-sortable" data-tabela="produtos">

          <thead>
            <tr>
              <th>Ordenar</th>
              <th class="yellow header headerSortDown">Título</th>
              <th class="header">Texto</th>
              <th class="red header">Ações</th>
            </tr>
          </thead>

          <tbody>
            <?php foreach ($registros as $key => $value): ?>

                <tr class="tr-row" id="row_<?=$value->id?>">
                  <td class="move-actions"><a href="#" class="btn btn-info btn-move">mover</a></td>
                  <td><?=$value->titulo?></td>
                  <td>
                    <?=word_limiter($value->texto, 15)?>
                    <div>
                      <?php if($value->lancamento == 1): ?>
                        <a href="#" class="btn btn-success btn-xs btn-marcar-lancamento" data-action="0" data-produto="<?php echo $value->id ?>" title="Desmarcar lançamento"><i class="icon-white icon-star"></i> lançamento</a>
                      <?php else: ?>
                        <a href="#" class="btn btn-default btn-xs btn-marcar-lancamento" data-action="1" data-produto="<?php echo $value->id ?>" title="Marcar como lançamento"><i class="icon-star-empty"></i> lançamento</a>
                      <?php endif; ?>
                    </div>
                  </td>
                  <td class="crud-actions" style="width:193px">
                    <a href="painel/<?=$this->router->class?>/imagens/<?=$value->id?>" class="btn">imagens</a>
                    <a href="painel/<?=$this->router->class?>/form/<?=$value->id?>" class="btn btn-primary">editar</a>
                    <a href="painel/<?=$this->router->class?>/excluir/<?=$value->id?>/<?=$value->id_produtos_categorias?>" class="btn btn-danger btn-delete">excluir</a>
                  </td>
                </tr>

            <?php endforeach ?>
          </tbody>

        </table>

        <?php if (isset($paginacao) && $paginacao): ?>
          <div class="pagination">
            <ul>
              <?=$paginacao?>
            </ul>
          </div>
        <?php endif ?>

      <?php else:?>

      	<h3>Nenhum Produto</h2>

      <?php endif ?>

    </div>
  </div>
