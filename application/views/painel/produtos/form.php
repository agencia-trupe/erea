<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>  

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Categoria<br>
			<select name="id_produtos_categorias" required id="id_produtos_categorias">
				<option value="">Selecione a categoria do Produto</option>
				<?php if ($categorias): ?>
					<?php foreach ($categorias as $key => $value): ?>
						<option value="<?=$value->id?>" <?if($value->id==$registro->id_produtos_categorias)echo" selected"?>><?=$value->titulo?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</label>
		
		<label>Título<br>
		<input type="text" name="titulo" required value="<?=$registro->titulo?>"></label>

		<label>
			<span id="lbl-img">Imagem</span>
			<br>
			<?if($registro->imagem):?>
				<img src="_imgs/produtos/thumbs/<?=$registro->imagem?>"><br>
			<?endif;?>
		<input type="file" name="userfile"></label>

		<label>Texto<br>
		<textarea name="texto" class="pequeno basico"><?=$registro->texto?></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Categoria<br>
			<select name="id_produtos_categorias" required id="id_produtos_categorias">
				<option value="">Selecione a categoria do Produto</option>
				<?php if ($categorias): ?>
					<?php foreach ($categorias as $key => $value): ?>
						<option value="<?=$value->id?>"><?=$value->titulo?></option>
					<?php endforeach ?>
				<?php endif ?>
			</select>
		</label>

		<label>Título<br>
		<input type="text" name="titulo" required></label>
		
		<label>
			<span id="lbl-img">Imagem</span>
			<br>
		<input type="file" name="userfile"></label>

		<label>Texto<br>
		<textarea name="texto" class="pequeno basico"></textarea></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>