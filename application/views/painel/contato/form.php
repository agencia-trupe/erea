<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

  <div class="page-header users-header">
    <h2>
      <?=$titulo?>
    </h2>
  </div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Telefone<br>
		<textarea name="telefone" class="pequeno basico"><?=$registro->telefone?></textarea></label>

		<label>Endereço<br>
		<textarea name="endereco" class="pequeno basico"><?=$registro->endereco?></textarea></label>

		<label>Código de Incorporação do Google Maps<br>
		<input type="text" name="gmaps" value="<?= htmlentities($registro->gmaps)?>"></label>

    <label>Link para Facebook<br>
		<input type="text" name="facebook" value="<?= htmlentities($registro->facebook)?>"></label>

    <label>Link para Instagram<br>
		<input type="text" name="instagram" value="<?= htmlentities($registro->instagram)?>"></label>

    <label>Link para Pinterest<br>
		<input type="text" name="pinterest" value="<?= htmlentities($registro->pinterest)?>"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Telefone<br>
		<textarea name="telefone" class="pequeno basico"></textarea></label>

		<label>Endereço<br>
		<textarea name="endereco" class="pequeno basico"></textarea></label>

		<label>Código de Incorporação do Google Maps<br>
		<input type="text" name="gmaps"></label>

    <label>Link para Facebook<br>
		<input type="text" name="facebook"></label>

    <label>Link para Instagram<br>
		<input type="text" name="instagram"></label>

    <label>Link para Pinterest<br>
		<input type="text" name="pinterest"></label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>
