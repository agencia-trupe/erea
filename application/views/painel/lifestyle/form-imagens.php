<div class="container top">

  <?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    <div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  <?elseif(isset($mostrarerro) && $mostrarerro):?>
    <div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  <?endif;?>

<?if ($registro): ?>

  <div class="page-header users-header">
    <h2>
      Lifestyle - Alterar Imagem
    </h2>
  </div>  

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterarImagem/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Imagem<br>
			<?if($registro->imagem):?>
				<img src="_imgs/lifestyle/<?=$registro->imagem?>"><br>
			<?endif;?>
		<input type="file" name="userfile"></label>

		<label>Orientação<br>
			<select name="orientacao">
				<option value="h" <?if($registro->orientacao=='h')echo" selected"?>>Horizontal</option>
				<option value="v" <?if($registro->orientacao=='v')echo" selected"?>>Vertical</option>
			</select>
		</label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>	
	
<?else: ?>

  <div class="page-header users-header">
    <h2>
      Lifestyle - Inserir Imagem
    </h2>
  </div>  

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserirImagem')?>" enctype="multipart/form-data">

		<div id="dialog"></div>
		
		<label>Imagem<br>
		<input type="file" name="userfile"></label>

		<label>Orientação<br>
			<select name="orientacao">
				<option value="h">Horizontal</option>
				<option value="v">Vertical</option>
			</select>
		</label>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>