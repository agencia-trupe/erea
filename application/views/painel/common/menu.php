<div class="navbar navbar-fixed-top">

    <div class="navbar-inner">

        <div class="container">

            <a href="painel/home" class="brand">Érea</a>

            <ul class="nav">

                <li <?if($this->router->class=='home')echo" class='active'"?>><a href="painel/home">Início</a></li>

                <li <?if($this->router->class=='destaque')echo" class='active'"?>><a href="painel/destaque">Destaque</a></li>

                <li <?if($this->router->class=='slides')echo" class='active'"?>><a href="painel/slides">Slides</a></li>

                <li <?if($this->router->class=='perfil')echo" class='active'"?>><a href="painel/perfil">Perfil</a></li>

                <li <?if($this->router->class=='produtos')echo" class='active'"?>><a href="painel/produtos">Produtos</a></li>

                <li <?if($this->router->class=='categorias')echo" class='active'"?>><a href="painel/categorias">Categorias</a></li>

                <li class="dropdown <?if($this->router->class=='lifestyle')echo"active"?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Lifestyle <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="painel/lifestyle/index">Texto</a></li>
                        <li><a href="painel/lifestyle/imagens">Imagens</a></li>
                    </ul>
                </li>

                <li <?if($this->router->class=='clippings')echo" class='active'"?>><a href="painel/clippings">Clipping</a></li>

                <li <?if($this->router->class=='novidades')echo" class='active'"?>><a href="painel/novidades">Novidades</a></li>

                <li <?if($this->router->class=='contato')echo" class='active'"?>><a href="painel/contato">Contato</a></li>

                <li class="dropdown <?if($this->router->class=='usuarios')echo"active"?>">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Sistema <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="painel/usuarios">Usuários</a></li>
                        <li><a href="painel/home/logout">Logout</a></li>
                    </ul>
                </li>

            </ul>

        </div>
    </div>
</div>
