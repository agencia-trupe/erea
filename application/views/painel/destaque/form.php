<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Frase de Destaque<br>
		<input type="text" name="frase" value="<?=$registro->frase?>" required autofocus></label>

    <hr>
      <label><input type="radio" name="ativa" value="1" <?php if($registro->ativa == '1') echo"checked" ?> required> Destaque Ativo (mostrar na home)</label>
      <label><input type="radio" name="ativa" value="0" <?php if($registro->ativa == '0') echo"checked" ?> required> Destaque Inativo (omitir na home)</label>
    <hr>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Salvar</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Frase de Destaque<br>
		<input type="text" name="frase" required autofocus></label>
    
    <hr>
      <label><input type="radio" name="ativa" value="1" required> Destaque Ativo (mostrar na home)</label>
      <label><input type="radio" name="ativa" value="0" required> Destaque Inativo (omitir na home)</label>
    <hr>

		<div class="form-actions">
        	<button class="btn btn-primary" type="submit">Inserir</button>
        	<button class="btn btn-voltar" type="reset">Voltar</button>
      	</div>
	</form>

<?endif ?>