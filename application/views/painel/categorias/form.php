<div class="container top">

	<?if(isset($mostrarsucesso) && $mostrarsucesso):?>
    	<div class="alert alert-block alert-success fade in" data-dismiss="alert"><?=$mostrarsucesso?></div>
  	<?elseif(isset($mostrarerro) && $mostrarerro):?>
    	<div class="alert alert-block alert-error fade in" data-dismiss="alert"><?=$mostrarerro?></div>
  	<?endif;?>

  	<div class="page-header users-header">
    	<h2>
      		<?=$titulo?>
    	</h2>
  	</div>

<?if ($registro): ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/alterar/'.$registro->id)?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>
			Título<br>
			<input type="text" name="titulo" required value="<?=$registro->titulo?>">
		</label>

		<label>
			Thumbnail<br>
			<select name="tipo_thumbnail" required>
				<option value="">Selecione</option>
				<option value="simples" <?=$registro->tipo_thumbnail == 'simples' ? 'selected' : ''?>>simples</option>
				<option value="duplo" <?=$registro->tipo_thumbnail == 'duplo' ? 'selected' : ''?>>duplo</option>
			</select>
		</label>

		<label>
			<input type="hidden" name="colecao" value="0">
			<input type="checkbox" name="colecao" value="1" <?=$registro->colecao ? 'checked' : ''?>>
			Coleção
		</label>

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Salvar</button>
			<button class="btn btn-voltar" type="reset">Voltar</button>
		</div>

	</form>

<?else: ?>

	<form method="post" action="<?=base_url('painel/'.$this->router->class.'/inserir')?>" enctype="multipart/form-data">

		<div id="dialog"></div>

		<label>Título<br>
		<input type="text" name="titulo" required autofocus></label>

		<label>
			Thumbnail<br>
			<select name="tipo_thumbnail" required>
				<option value="">Selecione</option>
				<option value="simples">simples</option>
				<option value="duplo">duplo</option>
			</select>
		</label>

		<label>
			<input type="hidden" name="colecao" value="0">
			<input type="checkbox" name="colecao" value="1">
			Coleção
		</label>

		<div class="form-actions">
			<button class="btn btn-primary" type="submit">Inserir</button>
			<button class="btn btn-voltar" type="reset">Voltar</button>
		</div>

	</form>

<?endif ?>
