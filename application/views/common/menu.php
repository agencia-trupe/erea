<header>

	<div class="centro">

		<a href="<?=base_url()?>" id="logo-link" title="Página Inicial"><img src="_imgs/layout/marca-erea.png" alt="Logo Érea"></a>

		<nav>
			<ul>
				<li><a href="home" title="Página Inicial" id="mn-home" <?if($this->router->class=='home')echo" class='ativo'"?>>home</a></li>
				<li><a href="perfil" title="Perfil" id="mn-perfil" <?if($this->router->class=='perfil')echo" class='ativo'"?>>perfil</a></li>
				<li id="mn-produtos" <?if($this->router->class=='produtos' && $this->router->method!='index') echo "class='comsub'" ?>>
					<a href="produtos" title="Produtos" <?if($this->router->class=='produtos')echo" class='ativo'"?>>
						produtos
					</a>
					<?php if ($categorias): ?>
						<ul class="submenu">
							<?php foreach ($categorias as $key => $value): ?>
								<li>
									<a href="produtos/categoria/<?=$value->slug?>" id="sub-<?=$value->slug?>" <?if(isset($slug) && $slug == $value->slug)echo"class='subativo'"?> title="<?=$value->titulo?>">
										<?=$value->titulo?>
									</a>
								</li>
							<?php endforeach ?>
						</ul>
					<?php endif ?>
				</li>
				<?php if($has_lancamento): ?>
					<li><a href="colecao-2018" title="Coleção 2018" id="mn-lancamentos" <?if(isset($slug) && $slug == 'colecao-2017')echo" class='ativo'"?>>coleção 2018</a></li>
				<?php endif; ?>
				<!-- <li><a href="lifestyle" title="Lifestyle" id="mn-lifestyle" <?if($this->router->class=='lifestyle')echo" class='ativo'"?>>lifestyle</a></li> -->
				<li><a href="clipping" title="Clipping" id="mn-clipping" <?if($this->router->class=='clipping')echo" class='ativo'"?>>clipping</a></li>
				<!-- <li><a href="novidades" title="Novidades" id="mn-novidades" <?if($this->router->class=='novidades')echo" class='ativo'"?>>novidades</a></li> -->
				<li><a href="contato" title="Contato" id="mn-contato" <?if($this->router->class=='contato')echo" class='ativo'"?>>contato</a></li>
				<li id="social">
					<?php if(isset($social[0]) && $social[0]->facebook): ?>
						<a href="<?php echo $social[0]->facebook ?>" title="Facebook" target="_blank" id="link-fb"><img src="_imgs/layout/erea-ico-fb.png"></a>
					<?php endif; ?>
					<?php if(isset($social[0]) && $social[0]->instagram): ?>
						<a href="<?php echo $social[0]->instagram ?>" title="Instagram" target="_blank" id="link-insta"><img src="_imgs/layout/erea-ico-insta.png"></a>
					<?php endif; ?>
					<?php if(isset($social[0]) && $social[0]->pinterest): ?>
						<a href="<?php echo $social[0]->pinterest ?>" title="Pinterest" target="_blank" id="link-pint"><img src="_imgs/layout/erea-ico-pint.png"></a>
					<?php endif; ?>
				</li>
			</ul>

		</nav>

	</div>

</header>

<div class="main main-<?=$this->router->class?> main-<?=$this->router->class?>-<?=$this->router->method?>">
