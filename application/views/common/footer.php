
    </div> <!-- fim da div main -->

    <footer>
        <div class="centro">
            <div class="direitos">&copy; <?=Date('Y')?> ÉREA &bull; Todos os direitos reservados</div>
            <a href="http://www.trupe.net" target="_blank" title="Criação de sites: Trupe Agência Criativa" class="trupe">
                <span>Criação de sites: Trupe Agência Criativa</span> <img src="_imgs/layout/kombi-trupe.png" alt="Criação de sites: Trupe Agência Criativa">
            </a>
        </div>
    </footer>


    <?if(ENVIRONMENT != 'development' && GOOGLE_ANALYTICS != FALSE):?>
        <script>
            window._gaq = [['_setAccount','UA<?=GOOGLE_ANALYTICS?>'],['_trackPageview'],['_trackPageLoadTime']];
            Modernizr.load({
                load: ('https:' == location.protocol ? '//ssl' : '//www') + '.google-analytics.com/ga.js'
            });
        </script>
    <?endif;?>

    <?JS(array('jquery-ui-1.10.3/ui/minified/jquery-ui.min','jquery.flip.min','cycle','front'))?>

</body>
</html>
