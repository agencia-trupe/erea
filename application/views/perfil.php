<div class="centro perfil">

	<?php if ($perfil->imagem1): ?>
		<div class="imagem"><img src="_imgs/perfil/<?=$perfil->imagem1?>"></div>		
	<?php endif ?>

	<div class="coluna-texto">
		<?php if ($perfil->texto): ?>
			<div class="texto1"><?=$perfil->texto?></div>
		<?php endif ?>
		<?php if ($perfil->perfil1): ?>
			<div class="texto2"><?=$perfil->perfil1?></div>
		<?php endif ?>
		<?php if ($perfil->perfil2): ?>
			<div class="texto3"><?=$perfil->perfil2?></div>
		<?php endif ?>
	</div>
	
</div>