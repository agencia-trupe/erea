<div class="lifestyle">

	<?php if ($lista): ?>
		<ul class="lista-lifestyle">

			<?php $cont_h = 1; $cont_v = 1; ?>

			<?php foreach ($lista as $key => $value): ?>

				<?php if ($value->orientacao == 'h'): ?>
					<li data-id="<?=($key + 1)?>" class="h h-<?=$cont_h?>">
						<img src="_imgs/lifestyle/<?=$value->imagem?>" alt="Lifestyle Erea">
					</li>
					<?php $cont_h++; ?>
				<?php else: ?>
					<li data-id="<?=($key + 1)?>" class="v v-<?=$cont_v?>">
						<img src="_imgs/lifestyle/<?=$value->imagem?>" alt="Lifestyle Erea">
					</li>
					<?php $cont_v++; ?>
				<?php endif; ?>
				
			<?php endforeach ?>

		</ul>

		<?php if (isset($texto[0])): ?>
			<div id="texto"><?=$texto[0]->texto?></div>
		<?php endif ?>


		<div class="loading fechado"></div>

		<?php if ($paginar): ?>
			<a href="#" id="vermais-lifestyle" title="Ver mais">></a>
		<?php endif ?>
	<?php endif ?>
	
	<input type="hidden" id="pagina-atual" value="<?=$pagina?>">
	<input type="hidden" id="num-lifestyle" value="<?=$num_resultados?>">

</div>