<div class="coluna mapa">
	<?php $mapa = preg_replace('~<br /><small>(.*)</small>~', '', $contato->gmaps);?>
	<?php $mapa = preg_replace("~ width=\"(\d+)\"~", ' width="100%"', $mapa); ?>
	<?php $mapa = preg_replace("~ height=\"(\d+)\"~", ' height="100%"', $mapa); ?>

	<?=$mapa?>
</div>
<div class="coluna form">
	<div class="pad">
		<div class="telefone"><?=$contato->telefone?></div>
		<div class="endereco"><?=$contato->endereco?></div>

		<?php if ($this->session->flashdata('contato_ok')): ?>

			<p class="agradecimento">
				Obrigado por entrar em contato!<br>
				Responderemos assim que possível.
			</p>

		<?php else: ?>

			<form id="form-contato" method="post" action="contato/enviar">
				FALE CONOSCO
				<input type="text" name="nome" placeholder="nome" required>
				<input type="email" name="email" placeholder="e-mail" required>
				<input type="text" name="telefone" placeholder="telefone">
				<textarea name="mensagem" required placeholder="mensagem"></textarea>
				<input type="submit" value="ENVIAR &raquo;">
			</form>

		<?php endif ?>
	</div>
</div>