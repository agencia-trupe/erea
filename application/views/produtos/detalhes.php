<div class="centro nome-categoria">
	produtos | <span><?=strtoupper($nome_categoria)?></span>
	<a href="produtos/categoria/<?=$slug_categoria?>">< voltar</a>
</div>

<div class="centro detalhes">

	<div id="ampliada">
		<?php if($variacoes && isset($variacoes[0])): ?>
			<img src="_imgs/produtos/<?=$variacoes[0]->imagem?>" alt="<?=$detalhes->titulo?>">
		<?php endif; ?>
	</div>

	<div id="variacoes">

		<?php if ($variacoes): ?>

			<a class="nav nav-up" href="#"></a>

			<ul class="thumb-viewport">
				<?php foreach ($variacoes as $key => $value): ?>

					<?php if($key == 0): ?>
						<li>
							<a class="thumb variacao-ativo" href="_imgs/produtos/<?=$value->imagem?>" target="_blank" title="Ampliar">
								<img src="_imgs/produtos/thumbs/<?=$value->imagem?>" alt="<?=$detalhes->titulo?>">
							</a>
						</li>
					<?php else: ?>
						<li>
							<a class="thumb" href="_imgs/produtos/<?=$value->imagem?>" target="_blank" title="Ampliar">
								<img src="_imgs/produtos/thumbs/<?=$value->imagem?>" alt="<?=$detalhes->titulo?>">
							</a>
						</li>
					<?php endif; ?>

				<?php endforeach ?>
			</ul>

			<a class="nav nav-down" href="#"></a>

		<?php else: ?>
			<div class="borda"></div>
		<?php endif ?>

	</div>

	<div id="texto">
		<h1><?=$detalhes->titulo?></h1>
		<?=$detalhes->texto?>
	</div>
</div>