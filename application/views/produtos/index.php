<div class="centro nome-categoria">
	produtos | <span><?= mb_strtoupper($nome_categoria)?></span>
</div>

<?php if ($produtos): ?>

	<?php if($tipo_categoria == 'duplo'): ?>

		<div class="produtos-container">
			<ul class="produtos-slide">
				<li class="duplo">
					<?php foreach ($produtos as $key => $value): ?>
						<? if($key%2==0 && $key>0) echo"</li><li class='duplo'>"; ?>
						<a href="produtos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
							<img src="_imgs/produtos/thumbswide/<?=$value->imagem?>" alt="<?=$value->titulo?>">
						</a>
					<?php endforeach ?>
				</li>
			</ul>
		</div>

	<?php else: ?>

		<div class="produtos-container">
			<ul class="produtos-slide">
				<li>
					<?php foreach ($produtos as $key => $value): ?>
						<? if($key%2==0 && $key>0) echo"</li><li>"; ?>
						<a href="produtos/detalhes/<?=$value->id?>" title="<?=$value->titulo?>">
							<img src="_imgs/produtos/thumbs/<?=$value->imagem?>" alt="<?=$value->titulo?>">
						</a>
					<?php endforeach ?>
				</li>
			</ul>
		</div>

	<?php endif; ?>

	<div class="centro slider-placeholder">
		<div id="slider"></div>
	</div>

<?php else: ?>
	<h2>Nenhum produto encontrado</h2>
<?php endif ?>
