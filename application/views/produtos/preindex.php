<ul id="lista-categorias">
	<?php if ($categorias): ?>
		<?php foreach ($categorias as $key => $value): ?>
			<li>
				<a href="produtos/categoria/<?=$value->slug?>" title="<?=mb_strtoupper($value->titulo)?>"><?=mb_strtoupper($value->titulo)?></a>
			</li>
		<?php endforeach ?>
	<?php endif ?>
</ul>