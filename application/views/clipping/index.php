<div class="centro clippings">
	<?php if ($clippings): ?>
		<?php foreach ($clippings as $key => $value): ?>
			<a href="clipping/ler/<?=$value->id?>" title="<?=$value->titulo?>">
				<div class="imagem">
					<img src="_imgs/clippings/capa/<?=$value->capa?>" alt="<?=$value->titulo?>">
					<div class="overlay"></div>
				</div>
				<?=$value->titulo?>
			</a>
		<?php endforeach ?>
	<?php endif ?>
</div>