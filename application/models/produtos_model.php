<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtos_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'produtos';
		$this->tabela_imagens = 'produtos_variacoes';
		$this->tabela_categorias = "produtos_categorias";

		$this->dados = array(
			'id_produtos_categorias',
			'titulo',
			'imagem',
			'texto',
			'lancamento'
		);
		$this->dados_tratados = array(
			'imagem' => $this->sobeImagem()
		);
	}

	function pegarPorCategoria($slug){
		$qry = <<<QRY
SELECT produtos.* FROM produtos LEFT JOIN produtos_categorias categorias ON
produtos.id_produtos_categorias = categorias.id
WHERE categorias.slug = '$slug'
ORDER BY produtos.ordem
QRY;
		return $this->db->query($qry)->result();
	}

	function pegarLancamentos(){
		return $this->db->order_by('ordem', 'asc')->where('lancamento', 1)->get($this->tabela)->result();
	}

	function pegarPaginadoPorCategoria($por_pagina, $inicio, $order_campo = 'ordem', $order = 'asc', $categoria){
		return $this->db->order_by($order_campo, $order)->where('id_produtos_categorias', $categoria)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function numeroResultados($categoria){
		return $this->db->get_where($this->tabela, array('id_produtos_categorias' => $categoria))->num_rows();
	}

	function categorias($id = false, $por_pagina = 5000, $inicio = 0){
		if($id){
			$qry = $this->db->get_where($this->tabela_categorias, array('id' => $id))->result();
			if(isset($qry[0]))
				return $qry[0];
			else
				return false;
		}else{
			return $this->db->order_by('titulo', 'ASC')->get($this->tabela_categorias, $por_pagina, $inicio)->result();
		}
	}

	function categoriasSemColecoes(){
		return $this->db->order_by('titulo', 'ASC')->get_where($this->tabela_categorias, array('colecao' => 0))->result();
	}

	function numeroCategorias(){
		return $this->db->get($this->tabela_categorias)->num_rows();
	}

	function categoriasPorSlug($slug = false){
		if($slug){
			$qry = $this->db->get_where($this->tabela_categorias, array('slug' => $slug, 'colecao' => 0))->result();
			if(isset($qry[0]))
				return $qry[0];
			else
				return false;
		}else{
			return $this->db->get($this->tabela_categorias)->result();
		}
	}

	function inserirCategoria(){
        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela_categorias, array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela_categorias, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		return $this->db->set('titulo', $this->input->post('titulo'))
				 		->set('slug', $slug)
				 		->insert($this->tabela_categorias);
	}

	function alterarCategoria($id){
        $slug = url_title($this->input->post("titulo"), '_', TRUE);
        $consulta_slug = $this->db->get_where($this->tabela_categorias, array('slug' => $slug))->num_rows();
        $add = 1;
        $check_slug = $slug;
        while($consulta_slug != 0){
            $check_slug = $slug.'_'.$add;
            $consulta_slug = $this->db->get_where($this->tabela_categorias, array('slug' => $check_slug))->num_rows();
            $add++;
        }
        $slug = $check_slug;

		return $this->db->set('titulo', $this->input->post('titulo'))
				 		->set('slug', $slug)
				 		->where('id', $id)
				 		->update($this->tabela_categorias);
	}

	function excluirCategoria($id){
		return $this->db->delete($this->tabela_categorias, array('id' => $id));
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/produtos/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	 $this->image_moo
	        	 		->load($original['dir'].$filename)
	        	 		->resize(470, 470)
	         	  		->save($original['dir'].$filename, TRUE)
	         	  		->resize(195, 195)
	         	  		->save($original['dir'].'thumbs/'.$filename, TRUE)
	         	  		->resize(390, 195)
	         	  		->save($original['dir'].'thumbswide/'.$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}
}
