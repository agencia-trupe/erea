<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Destaque_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'destaque';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('frase', 'ativa');
		$this->dados_tratados = array();
	}
}