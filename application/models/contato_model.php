<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'contato';

		$this->dados = array(
			'gmaps',
			'telefone',
			'endereco',
			'facebook',
			'instagram',
			'pinterest'
		);
		$this->dados_tratados = array(
			'gmaps' => isset($_POST['gmaps']) ? html_entity_decode($_POST['gmaps']) : ''
		);
	}


}
