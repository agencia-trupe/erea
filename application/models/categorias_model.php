<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categorias_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'produtos_categorias';
		$this->dados = array('titulo', 'slug', 'tipo_thumbnail', 'colecao');
		$this->dados_tratados = array();
	}

	function pegarPaginado($por_pagina, $inicio, $order_campo = 'titulo', $order = 'ASC'){
		return $this->db->order_by($order_campo, $order)->get($this->tabela, $por_pagina, $inicio)->result();
	}

	function inserir(){
		$slug = url_title($this->input->post("titulo"), '_', TRUE);
		$consulta_slug = $this->db->get_where($this->tabela, array('slug' => $slug))->num_rows();
		$add = 1;
		$check_slug = $slug;
		while($consulta_slug != 0){
				$check_slug = $slug.'_'.$add;
				$consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
				$add++;
		}
		$slug = $check_slug;

		return $this->db
			->set('titulo', $this->input->post('titulo'))
			->set('tipo_thumbnail', $this->input->post('tipo_thumbnail'))
			->set('colecao', $this->input->post('colecao'))
			->set('slug', $slug)
			->insert($this->tabela);
	}

	function alterar($id){
		$categoria = $this->pegarPorId($id);
		$slug = url_title($this->input->post("titulo"), '_', TRUE);

		if ($slug != $categoria->slug) {
			$consulta_slug = $this->db->get_where($this->tabela, array('slug' => $slug))->num_rows();
			$add = 1;
			$check_slug = $slug;
			while($consulta_slug != 0){
					$check_slug = $slug.'_'.$add;
					$consulta_slug = $this->db->get_where($this->tabela, array('slug' => $check_slug))->num_rows();
					$add++;
			}
			$slug = $check_slug;
		}

		return $this->db
			->set('titulo', $this->input->post('titulo'))
			->set('tipo_thumbnail', $this->input->post('tipo_thumbnail'))
			->set('colecao', $this->input->post('colecao'))
			->set('slug', $slug)
			->where('id', $id)
			->update($this->tabela);
	}

	function excluir($id){
		$produtos = $this->db->get_where('produtos', array('id_produtos_categorias' => $id))->num_rows();

		if($this->pegarPorId($id) !== FALSE && $produtos == 0){
			return $this->db->where('id', $id)->delete($this->tabela);
		}
	}

}
