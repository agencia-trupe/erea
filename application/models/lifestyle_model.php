<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lifestyle_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'lifestyle_texto';
		$this->tabela_imagens = 'lifestyle_imagens';

		$this->dados = array('texto');
		$this->dados_tratados = array();
	}

	function numeroImagens(){
		return $this->db->get($this->tabela_imagens)->num_rows();
	}

	function pegarImagensPaginado($por_pagina, $inicio){
		return $this->db->get($this->tabela_imagens, $por_pagina, $inicio)->result();
	}

	function pegarImagemPorId($id){
		$qry = $this->db->get_where($this->tabela_imagens, array('id' => $id))->result();
		if(isset($qry[0]))
			return $qry[0];
		else
			return FALSE;
	}

	function inserirImagem(){
		$this->db->set('imagem', $this->sobeImagem('userfile', $this->input->post('orientacao')))
				 ->set('orientacao', $this->input->post('orientacao'));

		return $this->db->insert($this->tabela_imagens);
	}

	function alterarImagem($id){
		if($this->pegarImagemPorId($id) !== FALSE){

			$imagem = $this->sobeImagem('userfile', $this->input->post('orientacao'));

			if($imagem)
				$this->db->set('imagem', $imagem);

			$this->db->set('orientacao', $this->input->post('orientacao'));

			return $this->db->where('id', $id)->update($this->tabela_imagens);
		}
	}

	function excluirImagem($id){
		if($this->pegarImagemPorId($id) !== FALSE){
			return $this->db->where('id', $id)->delete($this->tabela_imagens);
		}
	}

	// Corte Horizontal: 330x220
	// Corte Vertical: 220x330
	function sobeImagem($campo = 'userfile', $corte = 'h'){
		$this->load->library('upload');

		$x = ($corte == 'h') ? 330 : 220;
		$y = ($corte == 'h') ? 220 : 330;

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/lifestyle/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	 $this->image_moo->load($original['dir'].$filename)
	         	  				 ->resize_crop($x,$y)
	        	 				 ->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return false;
		}
	}
}