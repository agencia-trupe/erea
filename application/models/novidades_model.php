<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Novidades_model extends MY_Model {

	function __construct(){
		parent::__construct();

		$this->tabela = 'novidades';
		//$this->tabela_imagens = 'tabela_imagens';

		$this->dados = array('titulo','data_ordenacao','data_divulgacao','imagem','texto');
		$this->dados_tratados = array(
			'data_ordenacao' => formataData($this->input->post('data_ordenacao'), 'br2mysql'),
			'data_divulgacao' => formataData($this->input->post('data_divulgacao'), 'br2mysql'),
			'imagem' => $this->sobeImagem()
		);
	}

	function alterar($id){
		if($this->pegarPorId($id) !== FALSE){
			foreach($this->dados as $k => $v){
				if(array_key_exists($v, $this->dados_tratados) && $this->dados_tratados[$v] !== FALSE)
					$this->db->set($v, $this->dados_tratados[$v]);
				elseif($this->input->post($v) !== FALSE)
					$this->db->set($v, $this->input->post($v));
			}
			$remover = $this->input->post('removerImagem');
			if($remover)
				$this->db->set('imagem', '');
			return $this->db->where('id', $id)->update($this->tabela);
		}
	}

	function sobeImagem($campo = 'userfile'){
		$this->load->library('upload');

		$original = array(
			'campo' => $campo,
			'dir' => '_imgs/novidades/'
		);
		$campo = $original['campo'];

		$uploadconfig = array(
		  'upload_path' => $original['dir'],
		  'allowed_types' => 'jpg|png|gif',
		  'max_size' => '0',
		  'max_width' => '0',
		  'min_width' => '0',
		  'max_height' => '0',
		  'min_height' => '0');

		$this->upload->initialize($uploadconfig);

		if(isset($_FILES[$campo]) && $_FILES[$campo]['error'] != 4){
		    if(!$this->upload->do_upload($campo)){
		    	die($this->upload->display_errors());
		    }else{
		        $arquivo = $this->upload->data();
		        $filename = url_title($arquivo['file_name'], 'underscore', true);
		        rename($original['dir'].$arquivo['file_name'] , $original['dir'].$filename);

	        	$this->image_moo->load($original['dir'].$filename)
	         	  				->resize(310,99999)
	         	  				->save($original['dir'].$filename, TRUE);

		        return $filename;
		    }
		}else{
		    return '';
		}
	}
}